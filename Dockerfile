FROM golang:1.13

LABEL maintaner="Sunjin Yun <sunjin110@gmail.com>"

# Set the Current Working Directory inside the container
# WORKDIR /app
WORKDIR /gitlab.com/sunjin/goserver

# Copy go mod and sum files
COPY go.mod go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Copy the source from the current directory to the Working Directory inside the container
COPY . .

# NUL file delete
# RUN find . -name "._*.go" | xargs rm

# Build the Go app https://qiita.com/ukinau/items/410f56b6d777ad1e4e90
RUN go build -o main .
# RUN go 


# Expose port 8080 to the outside world
EXPOSE 8080 3306

# Command to run the executable
CMD ["./main"]