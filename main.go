package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/sunjin/goserver/controller"
)

func main() {

	router := gin.Default()
	router.LoadHTMLGlob("templates/*.html")

	router.GET("/", controller.TodoTop)
	router.POST("/create", controller.TodoCreate)
	router.GET("/detail/:id", controller.TodoDetail)
	router.POST("/update/:id", controller.TodoUpdate)
	router.POST("/delete/:id", controller.TodoDelete)
	// router.GET("/delete/:id", controller.TodoDeleteCheck)

	// static fileの指定
	router.Static("/public/css/", "./public/css")
	// router.Static("/public/js/", "./public/js/")
	// router.Static("/public/fonts/", "./public/fonts/")
	router.Static("/public/img/", "./public/img/")

	fmt.Println("===== server start =====")
	router.Run(":8080")

}
