package controller

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/sunjin/goserver/service"
)

// TodoTop is todo app top view
func TodoTop(ctx *gin.Context) {

	fmt.Println("===>> /")
	message := service.TodoTop()
	todos := service.GetTodoAll()

	ctx.HTML(http.StatusOK, "index.html", gin.H{
		"message": message,
		"todos":   todos,
	})
}

// TodoDetail is todo detail view
func TodoDetail(ctx *gin.Context) {
	fmt.Println("===>> /detail")
	n := ctx.Param("id")
	id, err := strconv.Atoi(n)
	if err != nil {
		panic(err.Error())
	}
	todo := service.GetTodoOne(id)
	ctx.HTML(http.StatusOK, "detail.html", gin.H{"todo": todo})
}

// TodoDeleteCheck .
func TodoDeleteCheck(ctx *gin.Context) {
	fmt.Println("===>> /delete")
	n := ctx.Param("id")
	id, err := strconv.Atoi(n)
	if err != nil {
		panic(err.Error())
	}
	todo := service.GetTodoOne(id)
	ctx.HTML(http.StatusOK, "delete.html", gin.H{"todo": todo})
}

// TodoDetail -> TODOの詳細を表示するview
// func TodoDetail(ctx *gin.Context) {
// 	fmt.Println("===>> /detail/:id")
// 	n := ctx.Param("id")
// 	id, err := strconv.Atoi(n)
// 	if err != nil {
// 		panic(err.Error())
// 	}

// 	todo :=
// }

// TodoCreate is create todo function. method post
func TodoCreate(ctx *gin.Context) {
	fmt.Println("===>> /create")

	// paarse
	err := ctx.Request.ParseForm()

	// check
	if err != nil {
		panic(err.Error())
	}

	// get data
	text := ctx.Request.Form["text"][0]
	status := ctx.Request.Form["status"][0]

	// create
	service.CreateTodo(text, status)

	// result
	ctx.Redirect(http.StatusFound, "/")
}

// TodoUpdate .
func TodoUpdate(ctx *gin.Context) {
	fmt.Println("===>> /update")

	n := ctx.Param("id")
	id, err := strconv.Atoi(n)
	if err != nil {
		panic(err.Error())
	}

	// parse
	err = ctx.Request.ParseForm()

	// check
	if err != nil {
		panic(err.Error())
	}

	// get data
	text := ctx.Request.Form["text"][0]
	status := ctx.Request.Form["status"][0]

	// update
	service.UpdateTodo(id, text, status)

	// result
	ctx.Redirect(http.StatusFound, "/")

}

// TodoDelete is TODOを削除するfunction
func TodoDelete(ctx *gin.Context) {
	fmt.Println("===>> /delete")

	n := ctx.Param("id")
	id, err := strconv.Atoi(n)
	if err != nil {
		panic(err.Error())
	}

	// delete
	service.DeleteTodo(id)

	// result
	ctx.Redirect(http.StatusFound, "/")
}
