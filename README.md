# TODOWebアプリ
golangとdockerを勉強するために作成しました。  
golangは、フレームワークGINを使用しています  
ディレクトリ

# 起動手順
docker-compose build --no-cache

docker-compose up

# 停止手順
docker-compose down

# 設定
docker-compose.ymlの  
```
    ports:
      - "5555:80"

```
ここの 5555を変更することでポートを変更できます

# 起動に失敗した時...
./Dockerfile内の
```
# RUN find . -name "._*.go" | xargs rm
```
のコメントアウトを外して、docker-compose build --no-cacheもう一度実行してみてください