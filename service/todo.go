package service

import "gitlab.com/sunjin/goserver/model"

// TodoTop .
func TodoTop() string {
	return "Gin Todo App"
}

// InitTodoDb .
func InitTodoDb() {
	model.DbTodoInit()
}

// CreateTodo todoを作る
func CreateTodo(text, status string) {
	model.DbInsert(text, status)
}

// UpdateTodo . todo update
func UpdateTodo(id int, text string, status string) {
	model.DbUpdate(id, text, status)
}

// DeleteTodo . todo delete
func DeleteTodo(id int) {
	model.DbDelete(id)
}

// GetTodoAll .
func GetTodoAll() []model.Todo {
	InitTodoDb()
	return model.DbGetAll()
}

// GetTodoOne .
func GetTodoOne(id int) model.Todo {
	return model.DbGetOne(id)
}
