package model

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// Todo is mysql table
type Todo struct {
	gorm.Model
	Text   string `json:text`
	Status string `json:status`
}

// 環境変数からデータを取得する、もし存在しない場合はデフォルトの値を指定する
func getParamString(param string, defaultValue string) string {
	env := os.Getenv(param)
	if env != "" {
		return env
	}
	return defaultValue
}

// Env is 環境変数からデータを取得する
// db connect
func dbConnect() *gorm.DB {

	DBMS := "mysql"
	USER := getParamString("MYSQL_USER", "user")
	PASS := getParamString("MYSQL_PASSWORD", "password")
	HOST := getParamString("MYSQL_DB_HOST", "127.0.0.1")
	PORT := getParamString("MYSQL_PORT", "3306")
	PROTOCOL := fmt.Sprintf("%s(%s:%s)", getParamString("MYSQL_PROTOCOL", "tcp"), HOST, PORT)
	DBNAME := getParamString("MYSQL_DATABASE", "sample_db")
	CHARSET := "utf8mb4"
	PARSETIME := "True"
	LOC := "Local"
	CONNECT := fmt.Sprintf("%s:%s@%s/%s?charset=%s&parseTime=%s&loc=%s", USER, PASS, PROTOCOL, DBNAME, CHARSET, PARSETIME, LOC)
	db, err := gorm.Open(DBMS, CONNECT)

	if err != nil {
		panic(err.Error())
	}
	return db
}

// DbTodoInit .
func DbTodoInit() {
	db := dbConnect()
	db.AutoMigrate(&Todo{})
}

// DbInsert .
func DbInsert(text, status string) {
	db := dbConnect()
	db.Create(&Todo{Text: text, Status: status})
	defer db.Close()
}

// DbUpdate .
func DbUpdate(id int, text string, status string) {
	db := dbConnect()
	var todo Todo
	db.First(&todo, id)
	todo.Text = text
	todo.Status = status
	db.Save(&todo)
	defer db.Close()
}

// DbDelete .
func DbDelete(id int) {
	db := dbConnect()
	var todo Todo
	db.First(&todo, id)
	db.Delete(&todo)
	defer db.Close()
}

// DbGetAll .
func DbGetAll() []Todo {
	db := dbConnect()
	var todoList []Todo
	db.Order("created_at desc").Find(&todoList)
	defer db.Close()
	return todoList
}

// DbGetOne is get todo find one
func DbGetOne(id int) Todo {
	db := dbConnect()
	var todo Todo
	db.First(&todo, id)
	defer db.Close()
	return todo
}
